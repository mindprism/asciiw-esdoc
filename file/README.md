## README

`C:\_\asciiw\._esdoc\file`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cfile) _`html` files and directories containing highlighted code `path/filename.ext`_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Cfile%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/._esdoc/file/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Cfile%5CREADME.html) `README.html`

