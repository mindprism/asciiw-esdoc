## README

`C:\_\asciiw\._esdoc`


[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc) _summary_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/._esdoc/) - bitbucket

### Anticipates

_none_

### Folders

- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `.git`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cast%5CREADME.md) `ast` - json files (`name.js.json`) and directories containing abstract syntax trees
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cclass%5CREADME.md) `class` - `html` files and directories containing class documentation `path/filename.ext~classname.html`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Ccss%5CREADME.md) `css` - Destination EsDoc Styles
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cfile%5CREADME.md) `file` - `html` files and directories containing highlighted code `path/filename.ext`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cfunction%5CREADME.md) `function` - Function Index
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cimage%5CREADME.md) `image` - Images for EsDoc
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cscript%5CREADME.md) `script` - Destination EsDoc built-in scripts, and additional scripts plus `manual.js` custom script
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Ctest-file%5CDesktop.ini)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Ctest-file%5CDesktop.ini) `test-file` - `html` files and directories containing highlighted code of test files `path/filename.test.ext`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cvariable%5CDesktop.ini)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5Cvariable%5CDesktop.ini) `variable` - EsDoc Variable Index HTML
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `_`

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Cbadge.svg) `badge.svg`
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Ccoverage.json) `coverage.json`
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Cdump.json) `dump.json`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Cidentifiers.html) `identifiers.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Cindex.html) `index.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Cpackage.json) `package.json`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Csource.html) `source.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C._esdoc%5Ctest.html) `test.html`

