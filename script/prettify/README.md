## README

`C:\_\asciiw\.assets\esdoc\script\prettify`

- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cprettify) _Source Prettify Code_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/.assets/esdoc/script/prettify/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/asciiw/esdoc/file/.assets/esdoc/script/prettify/prettify.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/asciiw/plato/files/esdoc_script_prettify_prettify_js/index.html) - plato

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CApache-License-%32.%30.txt) `Apache-License-2.0.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cprettify%5Cprettify.js) `prettify.js` - Library, has css dependency
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CREADME.html) `README.html`

